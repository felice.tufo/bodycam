; The name of the installer
Name "BodycamManagerSetup"

; The version of the BodycamService will be used as installation version
!getdllversion "..\BodycamManager\BodycamService\bin\Release\net6.0\BodycamService.exe" snmver

OutFile "BodycamManagerSetup-${snmver1}.${snmver2}.${snmver3}.${snmver4}.exe"

; The default installation directory
InstallDir $PROGRAMFILES64\CSI\BodycamManager

; Request application privileges
RequestExecutionLevel admin

;--------------------------------
; Pages

Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------
Section "Install"

  SectionIn RO
  
  ExecWait '"C:\Windows\System32\schtasks.exe" /end /tn BodycamManager'
  Pop $0
  ExecWait '"C:\Windows\System32\schtasks.exe" /delete /tn BodycamManager'
  Pop $0
  
  SetOutPath $INSTDIR\Service
  File /r "..\BodycamManager\BodycamService\bin\Release\net6.0\*"
  Delete $INSTDIR\Service\BodycamService.json
  Rename $INSTDIR\Service\BodycamService.Release.json $INSTDIR\Service\BodycamService.json

  ;SetOutPath $INSTDIR\Plugins\TestPlugin
  ;File /r "Publish\Plugins\TestPlugin\*"

  SetOutPath $INSTDIR\Plugins\IonodesPlugin
  File /r "..\Ionodes\IonodesPlugin\bin\Release\net6.0\*"
  Delete $INSTDIR\Plugins\IonodesPlugin\IonodesPlugin.json
  Rename $INSTDIR\Plugins\IonodesPlugin\IonodesPlugin.Release.json $INSTDIR\plugins\IonodesPlugin\IonodesPlugin.json
  
  ; Write the uninstall regitry keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\BodycamManager"   "DisplayName"     "BodycamManager"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\BodycamManager"   "Publisher"       "CSI"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\BodycamManager"   "DisplayVersion"  ${snmver1}.${snmver2}.${snmver3}.${snmver4}
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\BodycamManager"   "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\BodycamManager" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\BodycamManager" "NoRepair" 1
  WriteUninstaller "uninstall.exe"
  
  GetFullPathName /SHORT $0 $INSTDIR
  ExecWait '"C:\Windows\System32\schtasks.exe" /create /sc ONLOGON /tn BodycamManager /tr $0\Service\BodycamService.exe'
  Pop $0
  
SectionEnd

;--------------------------------
Section "Uninstall"

  ExecWait '"C:\Windows\System32\schtasks.exe" /end /tn BodycamManager'
  Pop $0
  ExecWait '"C:\Windows\System32\schtasks.exe" /delete /tn BodycamManager'
  Pop $0
  
  ; Remove files and uninstaller
  RMDir  /r $INSTDIR
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\BodycamManager"

SectionEnd
