﻿namespace BodycamService
{
    /// <summary>
    /// The macro categories in which the different errors are grouped
    /// </summary>
    public enum ErrorCategory
    {
        NoError,
        RecordingError,
        StreamingError,
        DownloadError,
        InternalError
    }

    internal class ErrorManager
    {
        /// <summary>
        /// Lock used for multithread safety
        /// </summary>
        private readonly object rwLock = new object();

        /// <summary>
        /// Last error detected
        /// </summary>
        private ErrorCategory LastError = ErrorCategory.NoError;

        /// <summary>
        /// Singleton instance of the class
        /// </summary>
        private static ErrorManager _instance = null;
        public static ErrorManager Instance 
        { 
            get
            { 
                if(_instance == null)
                {
                    _instance = new ErrorManager();
                }
                return _instance; 
            }
        }
        private ErrorManager() { }

        /// <summary>
        /// Returns the last error category
        /// </summary>
        /// <returns>The category of the last error detected</returns>
        public ErrorCategory GetError()
        {
            lock (rwLock)
            {
                return LastError;
            }
        }

        /// <summary>
        /// Sets the last error category
        /// </summary>
        public void SetError(ErrorCategory err)
        {
            lock (rwLock)
            {
                LastError = err;
            }
        }

        /// <summary>
        /// Clears any errors
        /// </summary>
        public void ClearError()
        {
            lock (rwLock)
            {
                LastError = ErrorCategory.NoError;
            }
        }

    }
}
