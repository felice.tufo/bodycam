﻿using System.Diagnostics;
using NLog;
using BodycamPluginBase;

namespace BodycamService
{
    internal class DownloadService
    {
        /// <summary>
        /// The logger
        /// </summary>
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Plugin to which to send bodycam commands
        /// </summary>
        private IBodycamPlugin plugin;

        /// <summary>
        /// The path to the working directory, used for file download and track extraction
        /// </summary>
        private string workDir;

        /// <summary>
        /// The path to the final publishing directory
        /// </summary>
        private string publishingDir;

        /// <summary>
        /// The path to the ffmpeg executable
        /// </summary>
        private string ffmpegPath;

        /// <summary>
        /// The path to the sqlite file
        /// </summary>
        private string sqliteFile;

        /// <summary>
        /// The days configured for cleanup
        /// </summary>
        private UInt16 cleanupDays;

        /// <summary>
        /// When true, .tmp files are not automatically deleted after track extraction
        /// </summary>
        private bool keepTmpFiles;

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="p">The current plugin reference</param>
        /// <param name="tmpDir">The temporary directory used to download and convert videos</param>
        /// <param name="pubDir">The final directory where to move video after conversion</param>
        /// <param name="ffmpegP">The path to the FFmpeg eecutable</param>
        /// <param name="sqliteP">The path to the sqlite file</param>
        /// <param name="cleanupD">The days used by the cleanup algorithm</param>
        /// <param name="keepTmp">When true, .tmp files are not automatically deleted after track extraction</param>
        public DownloadService(IBodycamPlugin p, string tmpDir, string pubDir, string ffmpegP, string sqliteP, UInt16 cleanupD, bool keepTmp)
        {
            plugin = p;
            workDir = tmpDir;
            publishingDir = pubDir;
            ffmpegPath = ffmpegP;
            sqliteFile = Path.GetFileName(sqliteP);
            cleanupDays = cleanupD;
            keepTmpFiles = keepTmp;
        }

        /// <summary>
        /// Main worker routine
        /// </summary>
        public void Download()
        {
            BodycamError err;

            /*
             * Get model and serialNo from the bodycam currently connected, they will be used in the successive calls to the DB.
             * It is assumed that the bodycam does not change before this function finishes executing.
             */
            string model;
            err = plugin.GetName(out model);
            if (err == BodycamError.GenericError)
            {
                logger.Error("Cannot get bodycam model");
                ErrorManager.Instance.SetError(ErrorCategory.DownloadError);
                return;
            }
            else if (err == BodycamError.ConnectionError)
            {
                return;
            }

            string serialNo;
            err = plugin.GetId(out serialNo);
            if (err == BodycamError.GenericError)
            {
                logger.Error("Cannot get bodycam serial number");
                ErrorManager.Instance.SetError(ErrorCategory.DownloadError);
                return;
            }
            else if (err == BodycamError.ConnectionError)
            {
                return;
            }

            ExecDownload(model, serialNo);
            ExecPublishing(model, serialNo);
            ExecCleanup(model, serialNo);
            ExecMaintenance();
        }

        /// <summary>
        /// - Gets the list of the clips stored in the bodycam storage, updating the DB
        /// - Retrieves the list of downloadable clips for the bodycam from the DB, trying
        ///   to download each one of them in the temp directory, and updating the DB with
        ///   the result of the operation. 
        ///   Not all clips in the bodycam storage needs to be downloaded: some of them may 
        ///   be already downloaded in the past but not deleted (so we are not going to download
        ///   them again, just retry the deletion at the end).
        /// </summary>
        /// <param name="model">The bodycam model</param>
        /// <param name="serialNo">The bodycam serial</param>
        private void ExecDownload(string model, string serialNo)
        {
            BodycamError err;

            List<string> recs;
            err = plugin.GetRecordings(out recs);
            if (err == BodycamError.GenericError)
            {
                logger.Error("Cannot download clip list from bodycam");
                ErrorManager.Instance.SetError(ErrorCategory.DownloadError);
                return;
            }
            else if (err == BodycamError.ConnectionError)
            {
                return;
            }

            foreach (string clip in recs)
            {
                SQLiteDataAccess.Instance.SetVideoDetected(model, serialNo, clip);
            }

            List<string> detectedClips = SQLiteDataAccess.Instance.GetDetectedVideos(model, serialNo);
            foreach (string clip in detectedClips)
            {
                err = BodycamError.NoError;

                // if bodycam is recording or streaming, stop downloading existing video as this may interfere
                // with recording or streaming performance
                bool isRecording = false;
                err = plugin.IsRecording(out isRecording);
                if ((err != BodycamError.NoError) || (isRecording == true))
                {
                    break;
                }

                bool isStreaming = false;
                err = plugin.IsWebcamModeEnabled(out isStreaming);
                if ((err != BodycamError.NoError) || (isStreaming == true))
                {
                    break;
                }

                if (recs.Contains(clip))
                {
                    err = plugin.DownloadRecording(clip, workDir);
                    if (err == BodycamError.NoError)
                    {
                        SQLiteDataAccess.Instance.SetVideoDownloaded(model, serialNo, clip);
                    }
                    else if (err == BodycamError.GenericError)
                    {
                        logger.Error("Cannot download clip {0} from bodycam", clip);
                        ErrorManager.Instance.SetError(ErrorCategory.DownloadError);
                    }
                    // Don't take any actions for a Connection Error, we'll retry next time
                }
                else
                {
                    // The video was detected in the past but it's not present anymore on the bodycam,
                    // maybe donwloaded and deleted by another BodycamManager... just mark it as missing
                    // to ignore it in the future
                    SQLiteDataAccess.Instance.SetVideoMissing(model, serialNo, clip);
                }
            }
        }

        /// <summary>
        /// - Reads from the DB all the downloaded videos
        /// - Creates a video-only and an audio only video from the original one
        /// - Finally moves the videos to the publishing directory, updating the DB
        /// </summary>
        /// <param name="model">The bodycam model</param>
        /// <param name="serialNo">The bodycam serial</param>
        private void ExecPublishing(string model, string serialNo)
        {
            List<string> dlist = SQLiteDataAccess.Instance.GetDownloadedVideos(model, serialNo);

            foreach (string clip in dlist)
            {
                try
                {
                    // if bodycam is recording or streaming, stop extracting tracks from videos as this may interfere
                    // with recording or streaming performance
                    bool isRecording = false;
                    BodycamError err = plugin.IsRecording(out isRecording);
                    if ((err != BodycamError.NoError) || (isRecording == true))
                    {
                        break;
                    }

                    bool isStreaming = false;
                    err = plugin.IsWebcamModeEnabled(out isStreaming);
                    if ((err != BodycamError.NoError) || (isStreaming == true))
                    {
                        break;
                    }

                    string clipname_working = Path.Combine(workDir, Path.GetFileNameWithoutExtension(clip));
                    string clipname_publishing = Path.Combine(publishingDir, Path.GetFileNameWithoutExtension(clip));
                    string ext = Path.GetExtension(clip);

                    string originalClip_working = Path.Combine(workDir, Path.GetFileName(clip));
                    string videoClip_working = clipname_working + "_video" + ext;
                    string audioClip_working = clipname_working + "_audio" + ext;
                    string originalTmp_working = originalClip_working + ".tmp";
                    string videoTmp_working = videoClip_working + ".tmp";
                    string audioTmp_working = audioClip_working + ".tmp";

                    string originalClip_publishing = Path.Combine(publishingDir, Path.GetFileName(clip));
                    string videoClip_publishing = clipname_publishing + "_video" + ext;
                    string audioClip_publishing = clipname_publishing + "_audio" + ext;
                    string originalTmp_publishing = originalClip_publishing + ".tmp";
                    string videoTmp_publishing = videoClip_publishing + ".tmp";
                    string audioTmp_publishing = audioClip_publishing + ".tmp";

                    if (File.Exists(originalClip_working) == false)
                    {
                        // Clip has status == downloaded in DB, but the file is not present in the directory
                        // Set video missing and continue with next file
                        SQLiteDataAccess.Instance.SetVideoMissing(model, serialNo, clip);
                        continue;
                    }

                    using Process p = new Process();
                    p.StartInfo.FileName = ffmpegPath;
                    // remove video track from container
                    p.StartInfo.Arguments = string.Format("-i {0} -vn -c copy -y {1}", originalClip_working, audioClip_working);
                    p.StartInfo.CreateNoWindow = true;
                    p.Start();
                    p.WaitForExit();

                    // remove audio track from container
                    p.StartInfo.Arguments = string.Format("-i {0} -an -c copy -y {1}", originalClip_working, videoClip_working);
                    p.Start();
                    p.WaitForExit();
                    
                    if (keepTmpFiles == false)
                    {
                        // Rename files adding .tmp extension
                        File.Move(originalClip_working, originalTmp_working);
                        File.Move(videoClip_working, videoTmp_working);
                        File.Move(audioClip_working, audioTmp_working);

                        // Move files to final destination
                        File.Move(originalTmp_working, originalTmp_publishing);
                        File.Move(videoTmp_working, videoTmp_publishing);
                        File.Move(audioTmp_working, audioTmp_publishing);

                        // Remove .tmp extension
                        File.Move(originalTmp_publishing, originalClip_publishing);
                        File.Move(videoTmp_publishing, videoClip_publishing);
                        File.Move(audioTmp_publishing, audioClip_publishing);
                    }
                    else
                    {
                        File.Copy(originalClip_working, originalTmp_working);
                        File.Copy(videoClip_working, videoTmp_working);
                        File.Copy(audioClip_working, audioTmp_working);

                        File.Copy(originalTmp_working, originalTmp_publishing);
                        File.Copy(videoTmp_working, videoTmp_publishing);
                        File.Copy(audioTmp_working, audioTmp_publishing);

                        File.Copy(originalTmp_publishing, originalClip_publishing);
                        File.Copy(videoTmp_publishing, videoClip_publishing);
                        File.Copy(audioTmp_publishing, audioClip_publishing);
                    }

                    SQLiteDataAccess.Instance.SetVideoPublished(model, serialNo, clip);
                }
                catch (Exception e)
                {
                    logger.Error("Cannot publish clip\n{0}", e.ToString());
                    ErrorManager.Instance.SetError(ErrorCategory.DownloadError);
                }
            }
        }

        /// <summary>
        /// Reads from the DB all the published videos for the current bodycam and tries to delete
        /// each one of them from the bodycam storage, updating the DB
        /// </summary>
        /// <param name="model">The bodycam model</param>
        /// <param name="serialNo">The bodycam serial</param>
        private void ExecCleanup(string model, string serialNo)
        {
            List<string> plist = SQLiteDataAccess.Instance.GetPublishedVideos(model, serialNo);

            BodycamError err;
            foreach (string clip in plist)
            {
                err = plugin.DeleteRecording(clip);
                if (err == BodycamError.GenericError)
                {
                    logger.Error("Cannot delete clip\n{0}", clip);
                    ErrorManager.Instance.SetError(ErrorCategory.DownloadError);
                    return;
                }
                else if (err == BodycamError.ConnectionError)
                {
                    break;
                }
                else
                {
                    SQLiteDataAccess.Instance.SetVideoDeleted(model, serialNo, clip);
                }
            }
        }

        /// <summary>
        /// - Deletes all the entries in the DB older than the configured cleanup days from the first 
        ///   time the video was detected
        /// - Deletes all the files in the working directory older than the configured cleanup days (ignoring
        ///   the sqlite DB file, if it is in the same directory
        /// </summary>
        private void ExecMaintenance()
        {
            if (cleanupDays <= 0)
            {
                return;
            }

            // cleanup DB
            DateTime dayLimit = DateTime.Now.Subtract(new TimeSpan(cleanupDays, 0, 0, 0));
            SQLiteDataAccess.Instance.DeleteExpiredFileEntries(dayLimit);

            // cleanup working dir
            string[] allfiles = Directory.GetFiles(workDir, "*.*", SearchOption.TopDirectoryOnly);
            foreach (string file in allfiles)
            {
                if (Path.GetFileName(file) == sqliteFile)
                {
                    continue;
                }

                DateTime creationTime = File.GetCreationTime(file);
                if (creationTime < dayLimit)
                {
                    logger.Warn("Deleting file '{0}' because it's older than {1} days", file, cleanupDays);
                    File.Delete(file);
                }
            }
        }

    }
}

