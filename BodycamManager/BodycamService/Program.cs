using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting.WindowsServices;
using NLog;
using NLog.Targets;
using NLog.Config;
using BodycamPluginBase;
using BodycamService;
using System.Runtime.InteropServices;

class Program
{
    /// <summary>
    /// Class used to serialize to JSON the "GET /" response
    /// </summary>
    class GetNameReturnObj
    {
        [JsonPropertyName("name")]
        public string name { get; set; }
        [JsonPropertyName("id")]
        public string id { get; set; }
    }

    /// <summary>
    /// Class used to serialize to JSON the "GET /status" response
    /// </summary>
    class GetStatusReturnObj
    {
        [JsonPropertyName("status")]
        public string status { get; set; }

        [JsonPropertyName("error")]
        public string error { get; set; }
    }

    /// <summary>
    /// Class used to serialize to JSON the "GET /properties" response
    /// </summary>
    class GetPropertiesReturnObj
    {
        [JsonPropertyName("properties")]
        public List<string> properties { get; set; }
    }

    /// <summary>
    /// Private reference to plugin
    /// </summary>
    private static IBodycamPlugin plugin = null;

    /// <summary>
    /// Imported Windows functions to hide the command window
    /// </summary>
    /// <returns></returns>
    [DllImport("kernel32.dll")]
    static extern IntPtr GetConsoleWindow();
    [DllImport("user32.dll")]
    static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);


    /// <summary>
    /// Main method
    /// </summary>
    /// <param name="args"></param>
    static void Main(string[] args)
    {
        // Hide cmd window when in RELEASE
#if !DEBUG
        const int SW_HIDE = 0;

        var handle = GetConsoleWindow();
        ShowWindow(handle, SW_HIDE);
#endif

        /*
        * Create WebApplicationBuilder adding service's own json configuration file
        */
        string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        string configFile = Path.Combine(assemblyFolder, "BodycamService.json");

        if (File.Exists(configFile) == false)
        {
            Console.WriteLine("Cannot found configuration file {0}, exiting...", configFile);
            return;
        }

        var builder = WebApplication.CreateBuilder(new WebApplicationOptions
        {
            Args = args,
            ContentRootPath = WindowsServiceHelpers.IsWindowsService() ? AppContext.BaseDirectory : default
        });

        builder.Configuration.AddJsonFile(configFile);

        /*
         * Configure logging
         */

        // try to read from configuration only the line to enable the debug log
        bool logDebug = false;
        bool.TryParse(builder.Configuration["EnableDebugLog"], out logDebug);

        var config = new LoggingConfiguration();
        var logEventLog = new EventLogTarget();
        if (logDebug == true)
        {
            config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, logEventLog);
        }
        else
        {
            config.AddRule(NLog.LogLevel.Info, NLog.LogLevel.Fatal, logEventLog);
        }

        logEventLog.Layout = "BODYCAM MANAGER\n${message}";
        LogManager.Configuration = config;
        Logger logger = LogManager.GetCurrentClassLogger();

        /*
         * Parse and validate the configuration 
         */
        string dllPath;
        string serviceUrl;
        string tempDir;
        string publishingDir;
        string sqlitePath;
        string ffmpegPath;
        UInt16 pollingTime;
        UInt16 cleanupDays;
        bool enableSwagger;
        bool keepTmpFiles;

        try
        {
            serviceUrl = builder.Configuration["ServiceUrl"];
            dllPath = builder.Configuration["DLLPath"];
            tempDir = builder.Configuration["TempDir"];
            publishingDir = builder.Configuration["PublishingDir"];
            sqlitePath = builder.Configuration["SQLitePath"];
            ffmpegPath = builder.Configuration["FFmpegPath"];

            if (string.IsNullOrEmpty(serviceUrl))
            {
                logger.Error("Missing ServiceUrl parameter in configuration file {0}", configFile);
                return;
            }

            if (string.IsNullOrEmpty(dllPath))
            {
                logger.Error("Missing DLLPath parameter in configuration file {0}", configFile);
                return;
            }

            if (Path.IsPathRooted(dllPath) == false)
            {
                dllPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), dllPath));
            }

            if (File.Exists(dllPath) == false)
            {
                logger.Error("Cannot find plugin at {0}", dllPath);
                return;
            }

            if (string.IsNullOrEmpty(tempDir))
            {
                logger.Error("Missing TempDir parameter in configuration file {0}", configFile);
                return;
            }

            if (string.IsNullOrEmpty(publishingDir))
            {
                logger.Error("Missing PublishingDir parameter in configuration file {0}", configFile);
                return;
            }

            if (string.IsNullOrEmpty(sqlitePath))
            {
                logger.Error("Missing SQLitePath parameter in configuration file {0}", configFile);
                return;
            }

            if (Path.IsPathRooted(sqlitePath) == false)
            {
                sqlitePath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), sqlitePath));
            }

            if (string.IsNullOrEmpty(ffmpegPath))
            {
                logger.Error("Missing FFmpegPath parameter in configuration file {0}", configFile);
                return;
            }

            if (Path.IsPathRooted(ffmpegPath) == false)
            {
                ffmpegPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), ffmpegPath));
            }

            if (File.Exists(ffmpegPath) == false)
            {
                logger.Error("Cannot find ffmpeg at {0}", ffmpegPath);
                return;
            }

            if (UInt16.TryParse(builder.Configuration["PollingTimeSec"], out pollingTime) == false)
            {
                logger.Error("Missing or invalid PollingTimeSec parameter in configuration file {0}", configFile);
                return;
            }

            if (UInt16.TryParse(builder.Configuration["CleanupDays"], out cleanupDays) == false)
            {
                logger.Error("Missing or invalid CleanupDays parameter in configuration file {0}", configFile);
                return;
            }

            if (bool.TryParse(builder.Configuration["EnableSwagger"], out enableSwagger) == false)
            {
                logger.Error("Missing or invalid EnableSwagger parameter in configuration file {0}", configFile);
                return;
            }
            if (bool.TryParse(builder.Configuration["KeepTmpFiles"], out keepTmpFiles) == false)
            {
                logger.Error("Missing or invalid KeepTmpFiles parameter in configuration file {0}", configFile);
                return;
            }

        }
        catch (Exception ex)
        {
            logger.Error("Invalid configuration file\n{0}", ex.ToString());
            return;
        }

        /*
         * Verify directories
         */
        if (CheckDir(tempDir) == false)
        {
            logger.Error("Cannot create directory with write permission: " + tempDir);
            return;
        }

        if (CheckDir(publishingDir) == false)
        {
            logger.Error("Cannot create directory with write permission: " + publishingDir);
            return;
        }

        string sqlitedir = Path.GetDirectoryName(sqlitePath);
        if (string.IsNullOrEmpty(sqlitedir) == false && CheckDir(sqlitedir) == false)
        {
            logger.Error("Cannot create directory with write permission: " + sqlitePath);
            return;
        }

        /*
         * Connect to the DB or create a new one
         */
        try
        {
            SQLiteDataAccess.Instance.OpenDB(sqlitePath);
        }
        catch (Exception ex)
        {
            logger.Error("Cannot open or create database\n" + ex.ToString());
            return;
        }


        /*
         * Search and instantiate the requested plugin
         */

        try
        {
            Assembly pluginAssembly = PluginLoadHelper.LoadPlugin(dllPath);
            plugin = PluginLoadHelper.CreatePluginInstance(pluginAssembly);
        }
        catch (Exception ex)
        {
            logger.Error("Cannot load plugin {0}\n{1}", dllPath, ex.ToString());
            return;
        }

        /*
         * Create the API routes
         */
        builder.Host.UseWindowsService();
        builder.WebHost.UseUrls(serviceUrl);
        builder.Services.AddScoped(x => new DownloadService(plugin, tempDir, publishingDir, ffmpegPath, sqlitePath, cleanupDays, keepTmpFiles));
        builder.Services.AddSingleton(provider => new PeriodicWorker(pollingTime, provider.GetService<IServiceScopeFactory>()));
        builder.Services.AddHostedService(provider => provider.GetRequiredService<PeriodicWorker>());
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();
        var app = builder.Build();

        if (enableSwagger == true)
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        /*
         * Finally configure routes using .NET 6.0 minimal APIs
         * https://learn.microsoft.com/en-us/aspnet/core/fundamentals/minimal-apis/overview?view=aspnetcore-6.0
         */

        /* 
         * GET / 
         */
        app.MapGet("/", () =>
        {
            string name;
            string id;

            BodycamError errname = BodycamError.NoError;
            BodycamError errid = BodycamError.NoError;

            errname = plugin.GetName(out name);
            if (errname == BodycamError.NoError)
            {
                errid = plugin.GetId(out id);
                if (errid == BodycamError.NoError)
                {
                    GetNameReturnObj retobj = new GetNameReturnObj();
                    retobj.name = name;
                    retobj.id = id;

                    logger.Debug("GET / =>\n{0}", JsonSerializer.Serialize(retobj));

                    return Results.Json(retobj);
                }
            }

            if ((errname == BodycamError.ConnectionError) || (errid == BodycamError.ConnectionError))
            {
                logger.Warn("GET / =>\nCannot read bodycam name and model - Bodycam unreachable");
                return Results.Problem(null, null, StatusCodes.Status503ServiceUnavailable);
            }
            else
            {
                // Set the error only if it is not a ConnectionError
                logger.Error("GET / =>\nCannot read bodycam name and model - Internal Error");
                ErrorManager.Instance.SetError(ErrorCategory.InternalError);
                return Results.Problem(null, null, StatusCodes.Status500InternalServerError);
            }
        });

        /* 
         * GET /status 
         */
        app.MapGet("/status", () =>
        {
            string status = "Idle";
            BodycamError errrecording = BodycamError.NoError;
            BodycamError errstreaming = BodycamError.NoError;
            bool isRecording;
            bool isStreaming;

            errrecording = plugin.IsRecording(out isRecording);
            if (errrecording == BodycamError.NoError)
            {
                errstreaming = plugin.IsWebcamModeEnabled(out isStreaming);
                if (errstreaming == BodycamError.NoError)
                {
                    if (isRecording == true)
                    {
                        status = "Recording";
                    }
                    else if (isStreaming == true)
                    {
                        status = "Streaming";
                    };

                    GetStatusReturnObj retobj = new GetStatusReturnObj();
                    retobj.status = status;
                    retobj.error = ErrorManager.Instance.GetError().ToString();

                    logger.Debug("GET /status =>\n{0}", JsonSerializer.Serialize(retobj));

                    return Results.Json(retobj);
                }
            }

            if ((errrecording == BodycamError.ConnectionError) || (errstreaming == BodycamError.ConnectionError))
            {
                status = "NotConnected";

                GetStatusReturnObj retobj = new GetStatusReturnObj();
                retobj.status = status;
                retobj.error = ErrorManager.Instance.GetError().ToString();

                logger.Debug("GET /status =>\n{0}", JsonSerializer.Serialize(retobj));

                return Results.Json(retobj);
            }
            else
            {
                // Set the error only if it is not a ConnectionError
                logger.Error("GET /status =>\nCannot read boydcam status - Internal Error");
                ErrorManager.Instance.SetError(ErrorCategory.InternalError);
                return Results.Problem(null, null, StatusCodes.Status500InternalServerError);
            }
        });

        /*
         * PUT /status
         */
        app.MapPut("/status/", () =>
        {
            ErrorManager.Instance.ClearError();

            logger.Debug("PUT /status =>\nAll errors cleared");

            return Results.Ok();
        });

        /*
         * GET /properties
         */
        app.MapGet("/properties", () =>
        {
            List<string> props;
            BodycamError err = plugin.GetProperties(out props);
            if (err == BodycamError.NoError)
            {
                GetPropertiesReturnObj retobj = new GetPropertiesReturnObj();
                retobj.properties = props;

                logger.Debug("GET /properties =>\n{0}", JsonSerializer.Serialize(retobj));

                return Results.Json(retobj);
            }
            else if (err == BodycamError.ConnectionError)
            {
                logger.Warn("GET /properties =>\nCannot read bodycam properties - Bodycam unreachable");
                return Results.Problem(null, null, StatusCodes.Status503ServiceUnavailable);
            }
            else
            {
                // Set the error only if it is not a ConnectionError
                logger.Error("GET /properties =>\nCannot read bodycam properties - Internal Error");
                ErrorManager.Instance.SetError(ErrorCategory.InternalError);
                return Results.Problem(null, null, StatusCodes.Status500InternalServerError);
            }
        });

        /*
         * GET /properties/{property_name}
         */
        app.MapGet("/properties/{propname}", (string propname) =>
        {
            string result;
            BodycamError err;

            err = plugin.GetProperty(propname, out result);
            if (err == BodycamError.NoError)
            {
                string retVal = "{\"" + propname + "\":\"" + result + "\"}";

                logger.Debug("GET /properties/{0} =>\n{1}", propname, retVal);

                return Results.Text(retVal);
            }
            else if (err == BodycamError.ConnectionError)
            {
                logger.Warn("GET /properties/{0} =>\nCannot read bodycam property '{0}' - Bodycam unreachable", propname);
                return Results.Problem(null, null, StatusCodes.Status503ServiceUnavailable);
            }
            else
            {
                // Set the error only if it is not a ConnectionError
                logger.Error("GET /properties/{0} =>\nCannot read bodycam property '{0}' - Internal error", propname);
                ErrorManager.Instance.SetError(ErrorCategory.InternalError);
                return Results.Problem(null, null, StatusCodes.Status500InternalServerError);
            }
        });

        /*
         * PUT /recording_status
         */
        app.MapPut("/recording_status", (bool start_rec, int? stop_after_sec) =>
        {
            BodycamError err;
            string logstr = "PUT /recording_status?start_rec=" + start_rec.ToString();
            if (stop_after_sec != null)
            {
                logstr += "&stop_after_sec=" + stop_after_sec.ToString();
            }
            logstr = logstr + " =>\n";

            if (start_rec == true)
            {
                int stopTimeoutSecs = 0;
                if ((stop_after_sec != null) && (stop_after_sec > 0))
                {
                    stopTimeoutSecs = (int)stop_after_sec;
                }

                err = plugin.StartRecording(stopTimeoutSecs);
            }
            else
            {
                err = plugin.StopRecording();
            }

            if (err == BodycamError.NoError)
            {
                logstr += "OK";
                logger.Debug(logstr);


                return Results.Ok();
            }
            else if (err == BodycamError.ConnectionError)
            {
                logstr += "Cannot complete requested operation - Bodycam unreachable";
                logger.Warn(logstr);
                return Results.Problem(null, null, StatusCodes.Status503ServiceUnavailable);
            }
            else
            {
                // Set the error only if it is not a ConnectionError
                logstr += "Cannot complete requested operation - Recording error";
                logger.Error(logstr);
                ErrorManager.Instance.SetError(ErrorCategory.RecordingError);
                return Results.Problem(null, null, StatusCodes.Status500InternalServerError);
            }
        });

        /*
         * PUT /streaming_status
         */
        app.MapPut("/streaming_status", (bool start_stream) =>
        {
            BodycamError err;

            string logstr = "PUT /streaming_status?start_stream=" + start_stream.ToString() + "=>\n";

            if (start_stream == true)
            {
                err = plugin.StartWebcamMode();
            }
            else
            {
                err = plugin.StopWebcamMode();
            }

            if (err == BodycamError.NoError)
            {
                logstr += "OK";
                logger.Debug(logstr);

                return Results.Ok();
            }
            else if (err == BodycamError.ConnectionError)
            {
                logstr += "Cannot complete requested operation - Bodycam unreachable";
                logger.Warn(logstr);
                return Results.Problem(null, null, StatusCodes.Status503ServiceUnavailable);
            }
            else
            {
                // Set the error only if it is not a ConnectionError
                logstr += "Cannot complete requested operation - Streaming Error";
                logger.Error(logstr);
                ErrorManager.Instance.SetError(ErrorCategory.StreamingError);
                return Results.Problem(null, null, StatusCodes.Status500InternalServerError);
            }
        });

        logger.Info(
@"Bodycam Manager service ready - Loaded configuration:
ServiceUrl -> {0}
DLLPath -> {1}
FFmpegPath -> {2}
TempDir -> {3}
PublishingDir -> {4}
SQLitePath -> {5}
PollingTimeSec -> {6}
CleanupDays -> {7}",
            serviceUrl, dllPath, ffmpegPath, tempDir, publishingDir, sqlitePath, pollingTime, cleanupDays
        );

        AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;

        // Launch
        app.Run();
    }

    /// <summary>
    /// Try to stop all bodycam activities on exit
    /// </summary>
    private static void CurrentDomain_ProcessExit(object? sender, EventArgs e)
    {
        plugin.StopRecording();
        plugin.StopWebcamMode();
    }

    /// <summary>
    /// Create new directory or verify if existing one has write permission
    /// </summary>
    /// <param name="path">The directory to create or verify</param>
    /// <returns>True if operation succeeds, false otherwise</returns>
    private static bool CheckDir(string path)
    {
        bool ret = true;
        try
        {
            if (Directory.Exists(path) == false)
            {
                Directory.CreateDirectory(path);
            }
            else
            {
                string filename = Path.Combine(path, "check.perm");
                using var file = File.Create(filename);
                file.Close();
                File.Delete(filename);
            }
        }
        catch
        {
            ret = false;
        }

        return ret;
    }

}