﻿using BodycamPluginBase;
using System.Reflection;
using System.Runtime.Loader;

namespace BodycamService
{
    /// <summary>
    /// Helper class to load a plugin implementing IBodycamPlugin interface
    /// </summary>
    class PluginLoadHelper
    {
        /// <summary>
        /// Given a path to a dll file, tries to resolve its dependencies, returning an Assembly object 
        /// </summary>
        /// <param name="path">The path to the dll file</param>
        /// <returns>The assembly object</returns>
        public static Assembly LoadPlugin(string path)
        {
            PluginLoadContext loadContext = new PluginLoadContext(path);
            return loadContext.LoadFromAssemblyPath(path);
        }

        /// <summary>
        /// Checks if an assembly implements the IBodycamPlugin interface and creates
        /// an object from the assembly
        /// </summary>
        /// <param name="assembly">The assembly to check</param>
        /// <returns>An object implementing the IBodycamPlugin interface</returns>
        public static IBodycamPlugin CreatePluginInstance(Assembly assembly)
        {
            IBodycamPlugin result = null;
            var types = assembly.GetTypes();
            foreach (Type type in types)
            {
                if (type.GetInterface(nameof(IBodycamPlugin)) != null)
                {
                    result = Activator.CreateInstance(type) as IBodycamPlugin;
                    if (result != null)
                    {
                        break;
                    }
                }
            }

            if (result == null)
            {
                string availableTypes = string.Join(",\n", assembly.GetTypes().Select(t => t.FullName));
                throw new ApplicationException(
                    $"Can't find any type which implements IBodycamPlugin in {assembly} from {assembly.Location}.\n" +
                    $"Available types: {availableTypes}");
            }

            return result;
        }
    }

    /// <summary>
    /// Helper class to load a .dll and all of its dependencies in the same folder into an Assembly
    /// See https://learn.microsoft.com/en-us/dotnet/core/tutorials/creating-app-with-plugin-support
    /// </summary>
    internal class PluginLoadContext : AssemblyLoadContext
    {
        private AssemblyDependencyResolver _resolver;
        private string pluginDir;

        public PluginLoadContext(string pluginPath)
        {
            pluginDir = Path.GetFullPath(Path.GetDirectoryName(pluginPath));
            _resolver = new AssemblyDependencyResolver(pluginPath);
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += new ResolveEventHandler(LoadFromSameFolder);
        }

        Assembly LoadFromSameFolder(object sender, ResolveEventArgs args)
        {
            string assemblyPath = Path.Combine(pluginDir, new AssemblyName(args.Name).Name + ".dll");
            if (File.Exists(assemblyPath) == false)
            {
                return null;
            }
            else
            {
                Assembly assembly = Assembly.LoadFrom(assemblyPath);
                return assembly;
            }
        }

        protected override Assembly Load(AssemblyName assemblyName)
        {
            string assemblyPath = _resolver.ResolveAssemblyToPath(assemblyName);
            if (assemblyPath != null)
            {
                return LoadFromAssemblyPath(assemblyPath);
            }

            return null;
        }

        protected override IntPtr LoadUnmanagedDll(string unmanagedDllName)
        {
            string libraryPath = _resolver.ResolveUnmanagedDllToPath(unmanagedDllName);
            if (libraryPath != null)
            {
                return LoadUnmanagedDllFromPath(libraryPath);
            }

            return IntPtr.Zero;
        }
    }
}