﻿using Microsoft.Data.Sqlite;
using NLog;

namespace BodycamService
{
    internal class SQLiteDataAccess
    {
        /// <summary>
        /// The logger
        /// </summary>
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The status codes used for videos
        /// WARNING: DO NOT CHANGE THE NUMBERING OF THE ITEMS WITHOUT RE-CREATING THE DB
        /// </summary>
        public enum FileStatus
        {
            DETECTED = 10,
            MISSING = 20,
            DOWNLOADED = 30,
            PUBLISHED = 40,
            DELETED = 50
        };

        /// <summary>
        /// The format of the timestamp saved into the DB
        /// </summary>
        private const string datetimeformat = "yyyy-MM-dd HH:mm:ss.fff";

        /// <summary>
        /// The connection to the DB
        /// </summary>
        private SqliteConnection conn = null;

        // Singleton to manage common access to DB
        private static SQLiteDataAccess _instance = null;
        public static SQLiteDataAccess Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SQLiteDataAccess();
                }

                return _instance;
            }
        }

        private SQLiteDataAccess() { }


        /// <summary>
        /// Opens connection to DB, creating the DB itself if the file is not found
        /// </summary>
        /// <param name="dbFile">The path to the sqlite file containing the DB</param>
        public void OpenDB(string dbFile)
        {
            if (conn != null)
            {
                conn.Close();
            }

            try
            {
                if (!File.Exists(dbFile))
                {
                    string sql =
                        @"CREATE TABLE 'Files' 
                          (
                            'FileId'                INTEGER NOT NULL PRIMARY KEY,
                            'BodycamModel'          TEXT    NOT NULL,
                            'BodycamSerialNo'	    TEXT    NOT NULL,
                            'SourceFile'	        TEXT    NOT NULL,
	                        'Status'                INTEGER NOT NULL,
                            'DetectedTimestamp'	    TEXT,
                            'MissingTimestamp'	    TEXT,
	                        'DownloadedTimestamp'	TEXT,
	                        'PublishedTimestamp'    TEXT,
                            'DeletedTimestamp'	    TEXT,
                            UNIQUE(BodycamModel, BodycamSerialNo, SourceFile)
                          );";

                    conn = new SqliteConnection($"Data Source={dbFile}");
                    conn.Open();

                    using SqliteCommand cmd = conn.CreateCommand();
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();

                    logger.Debug("Created {0}; connection open", dbFile);
                }
                else
                {
                    conn = new SqliteConnection($"Data Source={dbFile}");
                    conn.Open();


                    logger.Debug("Connected to {0}", dbFile);
                }
            }
            catch
            {
                if (conn != null)
                {
                    conn.Close();
                }

                throw;
            }
        }

        /// <summary>
        /// Insert a new video for the given bodycam model and serialNo with status DETECTED, if not already existent
        /// </summary>
        /// <param name="bodycamModel">The bodycam model</param>
        /// <param name="bodycamSerial">The bodycam serial number</param>
        /// <param name="clipname">The name of the video</param>
        public void SetVideoDetected(string bodycamModel, string bodycamSerial, string clipname)
        {
            string sql =
                @"INSERT INTO Files (BodycamModel, BodycamSerialNo, SourceFile, Status, DetectedTimestamp)
                  SELECT @BodycamModel, @BodycamSerialNo, @SourceFile, @Status, @DetectedTimestamp
                  WHERE NOT EXISTS (
                    SELECT * FROM Files WHERE BodycamModel=@BodycamModel AND BodycamSerialNo=@BodycamSerialNo AND SourceFile=@SourceFile
                  );";

            string tstamp = DateTime.Now.ToString(datetimeformat);

            using SqliteCommand cmd = conn.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@BodycamModel", bodycamModel);
            cmd.Parameters.AddWithValue("@BodycamSerialNo", bodycamSerial);
            cmd.Parameters.AddWithValue("@SourceFile", clipname);
            cmd.Parameters.AddWithValue("@Status", FileStatus.DETECTED);
            cmd.Parameters.AddWithValue("@DetectedTimestamp", tstamp);
            cmd.ExecuteNonQuery();

            logger.Debug("Executed SetVideoDetected() with bodycamModel='{0}', bodycamSerial='{1}', clipname='{2}', timestamp='{3}'",
                bodycamModel, bodycamSerial, clipname, tstamp);
        }

        /// <summary>
        /// Updates the status of the video, for the given bodycam model and serialNo, to MISSING
        /// </summary>
        /// <param name="bodycamModel">The bodycam model</param>
        /// <param name="bodycamSerial">The bodycam serial number</param>
        /// <param name="clipname">The name of the video</param>
        public void SetVideoMissing(string bodycamModel, string bodycamSerial, string clipname)
        {
            string sql = @"UPDATE Files SET Status=@NewStatus, MissingTimestamp=@MissingTimestamp 
                               WHERE BodycamModel=@BodycamModel AND BodycamSerialNo=@BodycamSerialNo AND SourceFile=@SourceFile;";

            string tstamp = DateTime.Now.ToString(datetimeformat);

            using SqliteCommand cmd = conn.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@BodycamModel", bodycamModel);
            cmd.Parameters.AddWithValue("@BodycamSerialNo", bodycamSerial);
            cmd.Parameters.AddWithValue("@SourceFile", clipname);
            cmd.Parameters.AddWithValue("@NewStatus", FileStatus.MISSING);
            cmd.Parameters.AddWithValue("@MissingTimestamp", tstamp);
            cmd.ExecuteNonQuery();

            logger.Debug("Executed SetVideoMissing() with bodycamModel='{0}', bodycamSerial='{1}', clipname='{2}', timestamp='{3}'",
                bodycamModel, bodycamSerial, clipname, tstamp);
        }

        /// <summary>
        /// Updates the status of the video, for the given bodycam model and serialNo, to DOWNLOADED
        /// </summary>
        /// <param name="bodycamModel">The bodycam model</param>
        /// <param name="bodycamSerial">The bodycam serial number</param>
        /// <param name="clipname">The name of the video</param>
        public void SetVideoDownloaded(string bodycamModel, string bodycamSerial, string clipname)
        {
            string sql = @"UPDATE Files SET Status=@NewStatus, DownloadedTimestamp=@DownloadedTimestamp 
                               WHERE BodycamModel=@BodycamModel AND BodycamSerialNo=@BodycamSerialNo AND SourceFile=@SourceFile;";

            string tstamp = DateTime.Now.ToString(datetimeformat);

            using SqliteCommand cmd = conn.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@BodycamModel", bodycamModel);
            cmd.Parameters.AddWithValue("@BodycamSerialNo", bodycamSerial);
            cmd.Parameters.AddWithValue("@SourceFile", clipname);
            cmd.Parameters.AddWithValue("@NewStatus", FileStatus.DOWNLOADED);
            cmd.Parameters.AddWithValue("@DownloadedTimestamp", tstamp);
            cmd.ExecuteNonQuery();

            logger.Debug("Executed SetVideoDownloaded() with bodycamModel='{0}', bodycamSerial='{1}', clipname='{2}', timestamp='{3}'",
                bodycamModel, bodycamSerial, clipname, tstamp);
        }

        /// <summary>
        /// Updates the status of the video, for the given bodycam model and serialNo, to PUBLISHED
        /// </summary>
        /// <param name="bodycamModel">The bodycam model</param>
        /// <param name="bodycamSerial">The bodycam serial number</param>
        /// <param name="clipname">The name of the video</param>
        public void SetVideoPublished(string bodycamModel, string bodycamSerial, string clipname)
        {
            string sql = @"UPDATE Files SET Status=@NewStatus, PublishedTimestamp=@PublishedTimestamp 
                               WHERE BodycamModel=@BodycamModel AND BodycamSerialNo=@BodycamSerialNo AND SourceFile=@SourceFile;";

            string tstamp = DateTime.Now.ToString(datetimeformat);

            using SqliteCommand cmd = conn.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@BodycamModel", bodycamModel);
            cmd.Parameters.AddWithValue("@BodycamSerialNo", bodycamSerial);
            cmd.Parameters.AddWithValue("@SourceFile", clipname);
            cmd.Parameters.AddWithValue("@NewStatus", FileStatus.PUBLISHED);
            cmd.Parameters.AddWithValue("@PublishedTimestamp", tstamp);
            cmd.ExecuteNonQuery();

            logger.Debug("Executed SetVideoPublished() with bodycamModel='{0}', bodycamSerial='{1}', clipname='{2}', timestamp='{3}'",
                bodycamModel, bodycamSerial, clipname, tstamp);
        }

        /// <summary>
        /// Updates the status of the video, for the given bodycam model and serialNo, to DELETED
        /// </summary>
        /// <param name="bodycamModel">The bodycam model</param>
        /// <param name="bodycamSerial">The bodycam serial number</param>
        /// <param name="clipname">The name of the video</param>
        public void SetVideoDeleted(string bodycamModel, string bodycamSerial, string clipname)
        {
            string sql = @"UPDATE Files SET Status=@NewStatus, DeletedTimestamp=@DeletedTimestamp 
                               WHERE BodycamModel=@BodycamModel AND BodycamSerialNo=@BodycamSerialNo AND SourceFile=@SourceFile;";

            string tstamp = DateTime.Now.ToString(datetimeformat);

            using SqliteCommand cmd = conn.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@BodycamModel", bodycamModel);
            cmd.Parameters.AddWithValue("@BodycamSerialNo", bodycamSerial);
            cmd.Parameters.AddWithValue("@SourceFile", clipname);
            cmd.Parameters.AddWithValue("@NewStatus", FileStatus.DELETED);
            cmd.Parameters.AddWithValue("@DeletedTimestamp", tstamp);
            cmd.ExecuteNonQuery();

            logger.Debug("Executed SetVideoDeleted() with bodycamModel='{0}', bodycamSerial='{1}', clipname='{2}', timestamp='{3}'",
                bodycamModel, bodycamSerial, clipname, tstamp);
        }

        /// <summary>
        /// Return the list of video havig status DETECTED for the given bodycam model and serialNo
        /// </summary>
        /// <param name="bodycamModel">The bodycam model</param>
        /// <param name="bodycamSerial">The bodycam serial number</param>
        /// <returns>The list of detected videos</returns>
        public List<string> GetDetectedVideos(string bodycamModel, string bodycamSerial)
        {
            List<string> videos = GetVideosByStatus(bodycamModel, bodycamSerial, FileStatus.DETECTED);
            logger.Debug("Executed GetDetectedVideos() with bodycamModel='{0}', bodycamSerial='{1}' =>\nvideos=[{2}]",
               bodycamModel, bodycamSerial, string.Join("\n", videos));

            return videos;
        }

        /// <summary>
        /// Return the list of video havig status DOWNLOADED for the given bodycam model and serialNo
        /// </summary>
        /// <param name="bodycamModel">The bodycam model</param>
        /// <param name="bodycamSerial">The bodycam serial number</param>
        /// <returns>The list of downloaded videos</returns>
        public List<string> GetDownloadedVideos(string bodycamModel, string bodycamSerial)
        {
            List<string> videos = GetVideosByStatus(bodycamModel, bodycamSerial, FileStatus.DOWNLOADED);
            logger.Debug("Executed GetDownloadedVideos() with bodycamModel='{0}', bodycamSerial='{1}' =>\nvideos=[{2}]",
               bodycamModel, bodycamSerial, string.Join("\n", videos));

            return videos;
        }

        /// <summary>
        /// Return the list of video havig status PUBLISHED for the given bodycam model and serialNo
        /// </summary>
        /// <param name="bodycamModel">The bodycam model</param>
        /// <param name="bodycamSerial">The bodycam serial number</param>
        /// <returns>The list of published videos</returns>
        public List<string> GetPublishedVideos(string bodycamModel, string bodycamSerial)
        {
            List<string> videos = GetVideosByStatus(bodycamModel, bodycamSerial, FileStatus.PUBLISHED);
            logger.Debug("Executed GetPublishedVideos() with bodycamModel='{0}', bodycamSerial='{1}' =>\nvideos=[{2}]",
               bodycamModel, bodycamSerial, string.Join("\n", videos));

            return videos;
        }

        /// <summary>
        /// Helper method to retrieve the list of videos having the given status for the given bodycam model and serialNo
        /// </summary>
        /// <param name="bodycamModel">The bodycam model</param>
        /// <param name="bodycamSerial">The bodycam serial number</param>
        /// <param name="status">The status of the video</param>
        /// <returns>The list of videos having the requested status</returns>
        private List<string> GetVideosByStatus(string bodycamModel, string bodycamSerial, FileStatus status)
        {
            List<string> clips = new List<string>();

            string sql =
               @"SELECT f.SourceFile 
                 FROM Files f
                 WHERE f.BodycamModel=@BodycamModel AND f.BodycamSerialNo=@BodycamSerialNo AND Status=@Status
                 ORDER BY DetectedTimestamp;";

            using SqliteCommand cmd = conn.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@BodycamModel", bodycamModel);
            cmd.Parameters.AddWithValue("@BodycamSerialNo", bodycamSerial);
            cmd.Parameters.AddWithValue("@Status", status);

            using SqliteDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                clips.Add(Convert.ToString(reader["SourceFile"]));
            }

            return clips;
        }

        /// <summary>
        /// Deletes from the DB all videos detected before a certain date
        /// </summary>
        /// <param name="dayLimit">The date before which a video can be deleted</param>
        public void DeleteExpiredFileEntries(DateTime dayLimit)
        {
            using SqliteCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM Files WHERE DetectedTimestamp<=@ExpirationDate;";
            cmd.Parameters.AddWithValue("@ExpirationDate", dayLimit.ToString(datetimeformat));
            cmd.ExecuteNonQuery();
        }
    }
}
