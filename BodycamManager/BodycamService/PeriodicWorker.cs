﻿using NLog;

namespace BodycamService
{
    /// <summary>
    /// Class used to create a Periodic Background Task to download the videos from the bodycam
    /// </summary>
    internal class PeriodicWorker : BackgroundService
    {
        private readonly IServiceScopeFactory factory;

        /// <summary>
        /// Wait period between calls to the DownloadService 
        /// </summary>
        private readonly TimeSpan period;

        /// <summary>
        /// The logger
        /// </summary>
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Class Constructor
        /// </summary>
        /// <param name="pollingTime">Seconds between each poll of the bodycam</param>
        /// <param name="fact">Factory used to create the periodic service</param>
        public PeriodicWorker(int pollingTime, IServiceScopeFactory fact)
        {
            period = TimeSpan.FromSeconds(pollingTime);
            factory = fact;
        }

        /// <summary>
        /// Method invoked by the framework to execute the background work
        /// </summary>
        /// <param name="stoppingToken">Cancellation Token</param>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using PeriodicTimer timer = new PeriodicTimer(period);
            while (!stoppingToken.IsCancellationRequested && await timer.WaitForNextTickAsync(stoppingToken))
            {
                try
                {
                    using IServiceScope serviceScope = factory.CreateScope();
                    DownloadService dloadService = serviceScope.ServiceProvider.GetRequiredService<DownloadService>();
                    logger.Debug("Starting DownloadService");

                    dloadService.Download();
                }
                catch (Exception e)
                {
                    logger.Error("Error while executing DownloadService\n{0}", e.ToString());
                    ErrorManager.Instance.SetError(ErrorCategory.DownloadError);
                }
            }
        }
    }
}
