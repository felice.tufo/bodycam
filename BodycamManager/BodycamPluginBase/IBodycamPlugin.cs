﻿namespace BodycamPluginBase
{
    /// <summary>
    /// Error codes allowed for the IBodycamPlugin interface
    /// </summary>
    public enum BodycamError
    {
        NoError,
        GenericError,
        ConnectionError
    }

    /// <summary>
    /// Interface that any plugin (that has to be dinamically loaded by the 
    /// BodycamManager) has to implement.
    /// All of these methods SHALL NOT throw exceptions, but rather return:
    /// - NoError if the operation completed successfully
    /// - GenericError if the operation did not complete successfully
    /// - ConnectionError if the bodycam did not answer to the request
    /// </summary>
    public interface IBodycamPlugin
    {
        /// <summary>
        /// Gets the name of the bodycam
        /// </summary>
        public BodycamError GetName(out string name);

        /// <summary>
        /// Gets the unique ID of the bodycam
        /// </summary>
        public BodycamError GetId(out string id);

        /// <summary>
        /// Gets the list of supported dynamic properties by the bodycam
        /// </summary>
        public BodycamError GetProperties(out List<string> properties);

        /// <summary>
        /// Gets the value of the specified dynamic property
        /// </summary>
        public BodycamError GetProperty(string property, out string value);

        /// <summary>
        /// Checks if the bodycam is recording a video
        /// </summary>
        public BodycamError IsRecording(out bool value);

        /// <summary>
        /// Tells the bodycam to immediately start recording a video, optionally stopping it
        /// after stopTimeoutSecs seconds (if > 0)
        /// </summary>
        public BodycamError StartRecording(int stopTimeoutSecs = 0);

        /// <summary>
        /// Tells the bodycam to immediately stop any video recording
        /// </summary>
        public BodycamError StopRecording();

        /// <summary>
        /// Gets the list of video recordings saved on the bodycam storage
        /// </summary>
        public BodycamError GetRecordings(out List<string> recordings);

        /// <summary>
        /// Downloads the named video from the bodycam storage to the told local path
        /// </summary>
        public BodycamError DownloadRecording(string name, string path);

        /// <summary>
        /// Deletes the named video from the bodycam storage
        /// </summary>
        public BodycamError DeleteRecording(string name);

        /// <summary>
        /// Executes all the needed operations to start the bodycam being usable as a webcam by Microsoft Teams
        /// </summary>
        public BodycamError StartWebcamMode();

        /// <summary>
        /// Executes all the needed operations to stop the bodycam being usable as a webcam by Microsoft Teams
        /// </summary>
        public BodycamError StopWebcamMode();

        /// <summary>
        /// Checks if the bodycam is currently usable as a webcam by Microsoft Teams
        /// </summary>
        public BodycamError IsWebcamModeEnabled(out bool value);
    }
}
