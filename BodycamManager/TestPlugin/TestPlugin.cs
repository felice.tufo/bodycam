﻿using BodycamPluginBase;

public class TestPlugin : IBodycamPlugin
{
    private List<string> Properties;
    private List<string> Recordings;
    private bool isRecording;
    private bool webcamEnabled;

    public TestPlugin()
    {
        Properties = new List<string>() { "property1", "property2" };
        Recordings = new List<string> { "file1", "file2" };
        isRecording = false;
        webcamEnabled = false;
    }

    public BodycamError GetName(out string name) 
    { 
        name = "Test Bodycam";
        return BodycamError.NoError;
    }

    public BodycamError GetId(out string id)
    {
        id = "12345";
        return BodycamError.NoError;
    }

    public BodycamError GetProperties(out List<string> props)
    { 
        props = Properties;
        return BodycamError.NoError; 
    }

    public BodycamError GetProperty(string name, out string value)
    {
        if (Properties.Contains(name))
        {
            value = name + "_val";
            return BodycamError.NoError;
        }
        else
        {
            value = String.Empty;
            return BodycamError.GenericError;
        }
    }

    public BodycamError IsRecording(out bool value)
    { 
        value = isRecording;
        return BodycamError.NoError;
    }

    public BodycamError StartRecording(int stopTimeoutSecs = 0) 
    { 
        if(webcamEnabled == false)
        {
            isRecording = true;
            return BodycamError.NoError;
        }
        return BodycamError.GenericError;        
    }

    public BodycamError StopRecording()
    {
        isRecording = false;
        return BodycamError.NoError;
    }

    public BodycamError DownloadRecording(string name, string path)
    {
        return BodycamError.NoError; 
    }

    public BodycamError DeleteRecording(string nane)
    {
        return BodycamError.NoError;
    }

    public BodycamError GetRecordings(out List<string> recs)
    { 
        recs = Recordings;
        return BodycamError.NoError;
    }

    public BodycamError StartWebcamMode()
    {
        if (isRecording == false)
        {
            webcamEnabled = true;
            return BodycamError.NoError;
        }
        return BodycamError.GenericError;
    }

    public BodycamError StopWebcamMode()
    {
        webcamEnabled = false;
        return BodycamError.NoError;
    }

    public BodycamError IsWebcamModeEnabled(out bool value)
    {
        value = webcamEnabled;
        return BodycamError.NoError;
    }
}
