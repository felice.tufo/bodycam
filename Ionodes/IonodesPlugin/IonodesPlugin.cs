﻿using System.Reflection;
using Microsoft.Extensions.Configuration;
using NLog;
using NLog.Config;
using NLog.Targets;
using BodycamPluginBase;

namespace Ionodes
{
    public class IonodesPlugin : IBodycamPlugin
    {
        /// <summary>
        /// String identifier for the battery level property
        /// </summary>
        private const string BATTERY_STR = "battery_level";

        /// <summary>
        /// String identifier for the free storage property
        /// </summary>
        private const string STORAGE_STR = "free_storage";

        /// <summary>
        /// Minimum duration of each recording
        /// </summary>
        private const int MIN_STOP_RECORDING = 6;

        /// <summary>
        /// Seconds to milliseconds conversion factor
        /// </summary>
        private const int SEC_TO_MSEC = 1000;

        /// <summary>
        /// The properties available for the IONODES PERCEPT bodycam
        /// </summary>
        private List<string> Properties = new List<string>() { BATTERY_STR, STORAGE_STR };

        /// <summary>
        /// The client for the bodycam REST API
        /// </summary>
        private ApiClient apiclient;

        /// <summary>
        /// The client for managing OBS Studio 
        /// </summary>
        private ObsClient obsclient;

        /// <summary>
        /// The logger
        /// </summary>
        private Logger logger;

        /// <summary>
        /// Class constructor
        /// </summary>
        public IonodesPlugin()
        {
            /*
             * Configure logging
             */
            var config = new LoggingConfiguration();
            var logEventLog = new EventLogTarget();
            config.AddRule(NLog.LogLevel.Info, NLog.LogLevel.Fatal, logEventLog);
            logEventLog.Layout = "IONODES PLUGIN\n${message}";
            LogManager.Configuration = config;
            logger = LogManager.GetCurrentClassLogger();

            /*
             * Read and validate configuration
             */
            string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string configFile = Path.Combine(assemblyFolder, "IonodesPlugin.json");

            var configuration = new ConfigurationBuilder()
                .AddJsonFile(configFile, false)
                .Build();

            string bodycamUrl = configuration["BodycamUrl"];
            string user = configuration["Username"];
            string pass = configuration["Password"];
            string obspath = configuration["OBSPath"];
            double apiTimeout;
            double downloadTimeout;
            Uri bodycamUri;

            if (string.IsNullOrEmpty(bodycamUrl))
            {
                throw new Exception("Missing BodycamUrl parameter in configuration file " + configFile);
            }

            try
            {
                bodycamUri = new Uri(bodycamUrl);
            }
            catch
            {
                throw new Exception(string.Format("BodycamUrl parameter '{0}' in configuration file {1} is not valid", bodycamUrl, configFile));
            }

            if (string.IsNullOrEmpty(user))
            {
                throw new Exception("Missing Username parameter in configuration file " + configFile);
            }

            if (string.IsNullOrEmpty(pass))
            {
                throw new Exception("Missing Password parameter in configuration file " + configFile);
            }

            if (string.IsNullOrEmpty(obspath))
            {
                throw new Exception("Missing OBSPath parameter in configuration file " + configFile);
            }

            if (Path.IsPathRooted(obspath) == false)
            {
                obspath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), obspath));
            }

            if (File.Exists(obspath) == false)
            {
                throw new Exception("Cannot find OBS studio at the configured plugin path: " + obspath);
            }

            if (double.TryParse(configuration["APITimeoutSec"], out apiTimeout) == false)
            {
                throw new Exception("Missing or invalid APITimeoutSec parameter in configuration file " + configFile);
            }

            if (double.TryParse(configuration["DownloadTimeoutSec"], out downloadTimeout) == false)
            {
                throw new Exception("Missing or invalid DownloadTimeoutSec parameter in configuration file " + configFile);
            }

            /*
             * Create clients
             */
            apiclient = new ApiClient(bodycamUri, user, pass, apiTimeout, downloadTimeout);
            obsclient = new ObsClient(obspath);

            logger.Info(
@"IONODES Plugin ready - Loaded configuration:
BodycamUrl -> {0}
Username -> {1}
Password -> {2}
OBSPath -> {3}
APITimeoutSec -> {4}
DownloadTimeoutSec -> {5}",
                        bodycamUrl, user, pass, obspath, apiTimeout, downloadTimeout
                    );
        }

        /// <summary>
        /// Gets the name of the bodycam
        /// </summary>
        /// <param name="name">The name read from the bodycam if the operation succeeds, an empty string otherwise</param>
        /// <returns>
        /// BodycamError.NoError if operation completed succesfully
        /// BodycamError.ConnectionError if the bodycam did not answer to the request
        /// BodycamError.GenericError if the operation failed
        /// </returns>
        public BodycamError GetName(out string name)
        {
            BodycamError err = BodycamError.NoError;
            try
            {
                name = apiclient.GetName();
            }
            catch (TaskCanceledException)
            {
                name = "";
                err = BodycamError.ConnectionError;
                logger.Warn("Cannot complete requested operation (GetName): bodycam not connected");
            }
            catch (Exception ex)
            {
                name = "";
                err = BodycamError.GenericError;
                logger.Error("Cannot complete requested operation (GetName)\n{0}", ex.ToString());
            }

            return err;
        }

        /// <summary>
        /// Gets the id of the bodycam
        /// </summary>
        /// <param name="id">The id read from the bodycam if the operation succeeds, an empty string otherwise</param>
        /// <returns>
        /// BodycamError.NoError if operation completed succesfully
        /// BodycamError.ConnectionError if the bodycam did not answer to the request
        /// BodycamError.GenericError if the operation failed
        /// </returns>
        public BodycamError GetId(out string id)
        {
            BodycamError err = BodycamError.NoError;
            try
            {
                id = apiclient.GetId();
            }
            catch (TaskCanceledException)
            {
                id = "";
                err = BodycamError.ConnectionError;
                logger.Warn("Cannot complete requested operation (GetId): bodycam not connected");
            }
            catch (Exception ex)
            {
                id = "";
                err = BodycamError.GenericError;
                logger.Error("Cannot complete requested operation (GetId)\n{0}", ex.ToString());
            }

            return err;
        }

        /// <summary>
        /// Gets the webcam properties
        /// </summary>
        /// <param name="props">The list of properties</param>
        /// <returns>
        /// BodycamError.NoError
        /// </returns>
        public BodycamError GetProperties(out List<string> props)
        {
            props = Properties;
            return BodycamError.NoError;
        }

        /// <summary>
        /// Gets the current value of a specific property
        /// </summary>
        /// <param name="name">The property for which to read the value</param>
        /// <param name="value">The value read from the bodycam if the operation succeeds, an empty string otherwise</param>
        /// <returns>
        /// BodycamError.NoError if operation completed succesfully
        /// BodycamError.ConnectionError if the bodycam did not answer to the request
        /// BodycamError.GenericError if the operation failed
        /// </returns>
        public BodycamError GetProperty(string name, out string value)
        {
            BodycamError err = BodycamError.NoError;

            try
            {
                if (name.Equals(BATTERY_STR, StringComparison.OrdinalIgnoreCase))
                {
                    value = apiclient.GetBattery();
                }
                else if (name.Equals(STORAGE_STR, StringComparison.OrdinalIgnoreCase))
                {
                    value = apiclient.GetStorage();
                }
                else
                {
                    value = "";
                    err = BodycamError.GenericError;
                    logger.Error("'{0}' is not a valid property", name);
                }
            }
            catch (TaskCanceledException)
            {
                value = "";
                err = BodycamError.ConnectionError;
                logger.Warn("Cannot complete requested operation (GetProperty): bodycam not connected");
            }
            catch (Exception ex)
            {
                value = "";
                err = BodycamError.GenericError;
                logger.Error("Cannot complete requested operation (GetProperty)\n{0}", ex.ToString());
            }

            return err;
        }

        /// <summary>
        /// Checks if the bodycam is recording a video
        /// </summary>
        /// <param name="value">True if the operation succeeded and the bodycam was recording a video, false otherwise</param>
        /// <returns>
        /// BodycamError.NoError if operation completed succesfully
        /// BodycamError.ConnectionError if the bodycam did not answer to the request
        /// BodycamError.GenericError if the operation failed
        /// </returns>
        public BodycamError IsRecording(out bool value)
        {
            BodycamError err = BodycamError.NoError;
            value = false;

            try
            {
                value = apiclient.IsRecording();
            }
            catch (TaskCanceledException)
            {
                err = BodycamError.ConnectionError;
                logger.Warn("Cannot complete requested operation (IsRecording): bodycam not connected");
            }
            catch (Exception ex)
            {
                err = BodycamError.GenericError;
                logger.Error("Cannot complete requested operation (IsRecording)\n{0}", ex.ToString());
            }

            return err;
        }

        /// <summary>
        /// Tells the bodycam to stop recording a new video, optionally starting a timer for automatically
        /// stopping the video when the timer expires
        /// </summary>
        /// <param name="stopTimeoutSecs">The seconds after which to stop the video automatically</param>
        /// <returns>
        /// BodycamError.NoError if operation completed succesfully
        /// BodycamError.ConnectionError if the bodycam did not answer to the request
        /// BodycamError.GenericError if the operation failed
        /// </returns>
        public BodycamError StartRecording(int stopTimeoutSecs = 0)
        {
            BodycamError err = BodycamError.NoError;
            try
            {
                apiclient.StartRecording();
                if (stopTimeoutSecs > 0)
                {
                    if (stopTimeoutSecs < MIN_STOP_RECORDING)
                    {
                        logger.Warn(
                            "Requested to start a video with a programmed stop after {0} seconds, but at least {1} second are needed; video may not be saved",
                            stopTimeoutSecs, MIN_STOP_RECORDING);
                    }

                    Task.Run(async () =>
                    {
                        await Task.Delay(stopTimeoutSecs * SEC_TO_MSEC);
                        try
                        {
                            apiclient.StopRecording();
                        }
                        catch (Exception ex)
                        {
                            logger.Error("Cannot complete programmed stop operation\n{0}", ex.ToString());
                        }
                    });
                }
            }
            catch (TaskCanceledException)
            {
                err = BodycamError.ConnectionError;
                logger.Warn("Cannot complete requested operation (StartRecording): bodycam not connected");
            }
            catch (Exception ex)
            {
                err = BodycamError.GenericError;
                logger.Error("Cannot complete requested operation (StartRecording)\n{0}", ex.ToString());
            }

            return err;
        }

        /// <summary>
        /// Tells the bodycam to stop recording the current video
        /// </summary>
        /// <returns>
        /// BodycamError.NoError if operation completed succesfully
        /// BodycamError.ConnectionError if the bodycam did not answer to the request
        /// BodycamError.GenericError if the operation failed
        /// </returns>
        public BodycamError StopRecording()
        {
            BodycamError err = BodycamError.NoError;

            try
            {
                apiclient.StopRecording();
            }
            catch (TaskCanceledException)
            {
                err = BodycamError.ConnectionError;
                logger.Warn("Cannot complete requested operation (StopRecording): bodycam not connected");
            }
            catch (Exception ex)
            {
                err = BodycamError.GenericError;
                logger.Error("Cannot complete requested operation (StopRecording)\n{0}", ex.ToString());
            }

            return err;
        }

        /// <summary>
        /// Download the requested video from the bodycam storage to the told path
        /// </summary>
        /// <param name="name">The name of the video to download</param>
        /// <param name="path">The path where to download the video</param>
        /// <returns>
        /// BodycamError.NoError if operation completed succesfully
        /// BodycamError.ConnectionError if the bodycam did not answer to the request
        /// BodycamError.GenericError if the operation failed
        /// </returns>
        public BodycamError DownloadRecording(string name, string path)
        {
            BodycamError err = BodycamError.NoError;

            try
            {
                apiclient.SaveRecording(name, path);
            }
            catch (TaskCanceledException)
            {
                err = BodycamError.ConnectionError;
                logger.Warn("Cannot complete requested operation (DownloadRecording): bodycam not connected");
            }
            catch (Exception ex)
            {
                err = BodycamError.GenericError;
                logger.Error("Cannot complete requested operation (DownloadRecording)\n{0}", ex.ToString());
            }

            return err;
        }

        /// <summary>
        /// Deletes a video from the bodycam storage
        /// </summary>
        /// <param name="name">The name of the video to download</param>
        /// <returns>
        /// BodycamError.NoError if operation completed succesfully
        /// BodycamError.ConnectionError if the bodycam did not answer to the request
        /// BodycamError.GenericError if the operation failed
        /// </returns>
        public BodycamError DeleteRecording(string name)
        {
            BodycamError err = BodycamError.NoError;

            try
            {
                apiclient.DeleteRecording(name);
            }
            catch (TaskCanceledException)
            {
                err = BodycamError.ConnectionError;
                logger.Warn("Cannot complete requested operation (DeleteRecording): bodycam not connected");
            }
            catch (Exception ex)
            {
                err = BodycamError.GenericError;
                logger.Error("Cannot complete requested operation (DeleteRecording)\n{0}", ex.ToString());
            }

            return err;
        }

        /// <summary>
        /// Gets the list of videos currently saved on the bodycam storage
        /// </summary>
        /// <param name="recs">The list of videos</param>
        /// <returns>
        /// BodycamError.NoError if operation completed succesfully
        /// BodycamError.ConnectionError if the bodycam did not answer to the request
        /// BodycamError.GenericError if the operation failed
        /// </returns>
        public BodycamError GetRecordings(out List<string> recs)
        {
            BodycamError err = BodycamError.NoError;

            try
            {
                recs = apiclient.ListRecordings();
            }
            catch (TaskCanceledException)
            {
                recs = null;
                err = BodycamError.ConnectionError;
                logger.Warn("Cannot complete requested operation (DeleteRecording): bodycam not connected");
            }
            catch (Exception ex)
            {
                recs = null;
                err = BodycamError.GenericError;
                logger.Error("Cannot complete requested operation (DeleteRecording)\n{0}", ex.ToString());
            }

            return err;
        }

        /// <summary>
        /// Starts OBS studio which redirects the bodycam RTSP stream to the OBS virtual camera
        /// </summary>
        /// <returns>
        /// BodycamError.NoError if operation completed succesfully
        /// BodycamError.GenericError if the operation failed
        /// </returns>
        public BodycamError StartWebcamMode()
        {
            BodycamError err = BodycamError.NoError;

            try
            {
                obsclient.StartObs();
            }
            catch (Exception ex)
            {
                err = BodycamError.GenericError;
                logger.Error("Cannot complete requested operation (StartWebcamMode)\n{0}", ex.ToString());
            }

            return err;
        }

        /// <summary>
        /// Stops OBS studio
        /// </summary>
        /// <returns>
        /// BodycamError.NoError if operation completed succesfully
        /// BodycamError.GenericError if the operation failed
        /// </returns>
        public BodycamError StopWebcamMode()
        {
            BodycamError err = BodycamError.NoError;

            try
            {
                obsclient.StopObs();
            }
            catch (Exception ex)
            {
                err = BodycamError.GenericError;
                logger.Error("Cannot complete requested operation (StopWebcamMode)\n{0}", ex.ToString());
            }

            return err;
        }

        /// <summary>
        /// Check if the bodycam is being used as a webcam by verifying that the bodycam is reachable
        /// and OBS studio is started
        /// </summary>
        /// <param name="value"></param>
        /// <returns>
        /// BodycamError.NoError if operation completed succesfully
        /// BodycamError.ConnectionError if the bodycam did not answer to the request
        /// BodycamError.GenericError if the operation failed
        /// </returns>
        public BodycamError IsWebcamModeEnabled(out bool value)
        {
            BodycamError err = BodycamError.NoError;

            try
            {
                // call IsRecording to make sure that bodycam is connected
                var _ = apiclient.IsRecording();
                value = obsclient.IsStreaming();
            }
            catch (TaskCanceledException)
            {
                value = false;
                err = BodycamError.ConnectionError;
                logger.Warn("Cannot complete requested operation (IsWebcamModeEnabled): bodycam not connected");
            }
            catch (Exception ex)
            {
                value = false;
                err = BodycamError.GenericError;
                logger.Error("Cannot complete requested operation (IsWebcamModeEnabled)\n{0}", ex.ToString());
            }

            return err;
        }
    }

}
