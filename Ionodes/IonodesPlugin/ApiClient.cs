﻿using System.Net;
using System.Xml.Linq;

namespace Ionodes
{
    public class ApiClient
    {
        /// <summary>
        /// Bytes to megabytes conversion factor
        /// </summary>
        private const int BYTES_IN_MEGABYTES = (1024 * 1024);

        /// <summary>
        /// The position of the free space information in the array of parameters
        /// returned by the API to read the device information
        /// </summary>
        private const int FREE_SPACE_POS = 4;

        /// <summary>
        /// The HTTP client used for all API calls, except the one to download videos
        /// </summary>
        private HttpClient client;

        /// <summary>
        /// The HTTP client user for downloading videos
        /// </summary>
        private HttpClient download_client;

        /// <summary>
        /// The base HTTP url of the bodycam APIs
        /// </summary>
        private Uri domain;

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="uri">The Bodycam API base URI</param>
        /// <param name="user">The username used to connect to the bodycam</param>
        /// <param name="pass">The password used to connect to the bodycam</param>
        /// <param name="apiTimeout">Seconds to wait for a API request to complete</param>
        /// <param name="downloadTimeout">Seconds to wait for a clip download to complete</param>
        public ApiClient(Uri uri, string user, string pass, double apiTimeout, double downloadTimeout)
        {
            domain = uri;
            var credCache = new CredentialCache();
            credCache.Add(domain, "Digest", new NetworkCredential(user, pass));

            client = new HttpClient(new HttpClientHandler { Credentials = credCache });
            client.Timeout = TimeSpan.FromSeconds(apiTimeout);

            download_client = new HttpClient(new HttpClientHandler { Credentials = credCache });
            download_client.Timeout = TimeSpan.FromSeconds(downloadTimeout);
        }

        /// <summary>
        /// Gets the name of the bodycam
        /// </summary>
        /// <returns>The string containing the name of the bodycam</returns>
        public string GetName()
        {
            var uri = new Uri(domain, "services/system.ion?sel=discover");
            using var request = new HttpRequestMessage(HttpMethod.Get, uri);

            using var response = client
                .Send(request)
                .EnsureSuccessStatusCode();

            using var stream = response.Content.ReadAsStream();
            using var reader = new StreamReader(stream);
            var responseBody = reader.ReadToEnd();
            var doc = XDocument.Parse(responseBody);
            var res = doc.Descendants("param")
                        .Where(x => x.Attribute("name").Value.Equals(("model"), StringComparison.OrdinalIgnoreCase))
                        .Select(q => q.Value)
                        .First();

            return res.ToString();
        }

        /// <summary>
        /// Gets the serial number of the bodycam
        /// </summary>
        /// <returns>The string containing the serial number of the bodycam</returns>
        public string GetId()
        {
            var uri = new Uri(domain, "services/system.ion?sel=discover");
            using var request = new HttpRequestMessage(HttpMethod.Get, uri);

            using var response = client
                .Send(request)
                .EnsureSuccessStatusCode();

            using var stream = response.Content.ReadAsStream();
            using var reader = new StreamReader(stream);
            var responseBody = reader.ReadToEnd();
            var doc = XDocument.Parse(responseBody);
            var res = doc.Descendants("param")
                        .Where(x => x.Attribute("name").Value.Equals(("serialnum"), StringComparison.OrdinalIgnoreCase))
                        .Select(q => q.Value)
                        .First();

            return res.ToString();
        }

        /// <summary>
        /// Gets the battery level of the bodycam
        /// </summary>
        /// <returns>The string containing the battery level of the bodycam</returns>
        public string GetBattery()
        {
            var uri = new Uri(domain, "services/system.ion?sel=paramlist&params=system.battery.*");
            using var request = new HttpRequestMessage(HttpMethod.Get, uri);

            using var response = client
                .Send(request)
                .EnsureSuccessStatusCode();

            using var stream = response.Content.ReadAsStream();
            using var reader = new StreamReader(stream);
            var responseBody = reader.ReadToEnd();
            var doc = XDocument.Parse(responseBody);
            var res = doc.Descendants("attribute")
                        .Where(x => x.Attribute("name").Value.Equals(("level"), StringComparison.OrdinalIgnoreCase))
                        .Select(q => q.Value)
                        .First();

            return res.ToString();
        }

        /// <summary>
        /// Gets the free storage of the bodycam
        /// </summary>
        /// <returns>The string containing the free storage of the bodycam, in Megabytes</returns>
        public string GetStorage()
        {
            var uri = new Uri(domain, "services/system.ion?sel=paramlist&params=system.storageinfo");
            using var request = new HttpRequestMessage(HttpMethod.Get, uri);

            using var response = client
                .Send(request)
                .EnsureSuccessStatusCode();

            using var stream = response.Content.ReadAsStream();
            using var reader = new StreamReader(stream);
            var responseBody = reader.ReadToEnd();
            var doc = XDocument.Parse(responseBody);
            var res = doc.Descendants("attribute")
                        .Where(x => x.Attribute("name").Value.Equals(("storageinfo"), StringComparison.OrdinalIgnoreCase))
                        .Select(q => q.Value)
                        .First();

            var fullstr = res.ToString();
            string[] tokenizedstr = fullstr.Split(";");

            return (Int64.Parse(tokenizedstr[FREE_SPACE_POS]) / BYTES_IN_MEGABYTES).ToString();
        }

        /// <summary>
        /// Checks is the bodycam is recording a video
        /// </summary>
        /// <returns>True is the bodycam is recording a video, false otherwise</returns>
        public bool IsRecording()
        {
            bool ret = false;

            var uri = new Uri(domain, "services/configuration.ion?sel=paramlist&params=videoinput_*.recording.recordingmode");
            using var request = new HttpRequestMessage(HttpMethod.Get, uri);

            using var response = client
                .Send(request)
                .EnsureSuccessStatusCode();

            using var stream = response.Content.ReadAsStream();
            using var reader = new StreamReader(stream);
            var responseBody = reader.ReadToEnd();
            var doc = XDocument.Parse(responseBody);
            var res = doc.Descendants("attribute")
                        .Where(x => x.Attribute("name").Value.Equals(("recordingmode"), StringComparison.OrdinalIgnoreCase))
                        .Select(q => q.Value)
                        .First();

            if (res.Contains("ondemand", StringComparison.OrdinalIgnoreCase))
            {
                ret = true;
            }

            return ret;
        }

        /// <summary>
        /// Tells the bodycam to start recording a new video
        /// </summary>
        public void StartRecording()
        {
            var uri = new Uri(domain, "services/media.ion?action=startondemandrecording&sources=videoinput_*");
            using var request = new HttpRequestMessage(HttpMethod.Post, uri);

            client.Send(request).EnsureSuccessStatusCode();
        }

        /// <summary>
        /// Tells the bodycam to stop recording the current video
        /// </summary>
        public void StopRecording()
        {
            var uri = new Uri(domain, "services/media.ion?action=stopondemandrecording&sources=videoinput_*");
            using var request = new HttpRequestMessage(HttpMethod.Post, uri);

            client.Send(request).EnsureSuccessStatusCode();
        }

        /// <summary>
        /// Gets the list of videos currently stored on the bodycam
        /// </summary>
        /// <returns>The list of videos</returns>
        public List<string> ListRecordings()
        {
            List<string> recs = new List<string>();

            var uri = new Uri(domain, "services/media.ion?sel=clipinfo&sources=videoinput_*");
            using var request = new HttpRequestMessage(HttpMethod.Get, uri);

            using var response = client
                .Send(request)
                .EnsureSuccessStatusCode();

            using var stream = response.Content.ReadAsStream();
            using var reader = new StreamReader(stream);
            var responseBody = reader.ReadToEnd();
            var doc = XDocument.Parse(responseBody);
            var clips = doc.Descendants("clip");

            foreach (var clip in clips)
            {
                recs.Add(clip.Value);
            }

            return recs;
        }

        /// <summary>
        /// Downloads a video from the bodycam storage, saving it in the told path
        /// </summary>
        /// <param name="clip">The video to be saved</param>
        /// <param name="path">The path where to save the video</param>
        public void SaveRecording(string clip, string path)
        {
            string realFile = Path.Combine(path, Path.GetFileName(clip));
            string tmpFile = realFile + ".part";

            using var fileStream = new FileStream(tmpFile, FileMode.Create);
            using var request = new HttpRequestMessage(HttpMethod.Get, clip);

            // here we use the download_client which should be configured
            // with a higher download timeout than the other API client
            using var response = download_client
                .Send(request)
                .EnsureSuccessStatusCode();

            using var responseStream = response.Content.ReadAsStream();
            responseStream.CopyTo(fileStream);
            responseStream.Flush();
            fileStream.Flush();
            fileStream.Close();

            File.Move(tmpFile, realFile);
        }

        /// <summary>
        /// Deletes a video from the bodycam storage
        /// </summary>
        /// <param name="clip">The name of the video to be deleted</param>
        public void DeleteRecording(string clip)
        {
            string name = Path.GetFileName(clip);
            var uri = new Uri(domain, "services/media.ion?action=deleteclips&clip=" + name);
            using var request = new HttpRequestMessage(HttpMethod.Post, uri);

            client.Send(request).EnsureSuccessStatusCode();
        }
    }
}
