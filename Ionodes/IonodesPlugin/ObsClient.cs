﻿using System.Diagnostics;

namespace Ionodes
{
    internal class ObsClient
    {
        /// <summary>
        /// The path to the obs executable
        /// </summary>
        private string exefile;

        /// <summary>
        /// The path to the obs executable
        /// </summary>
        private string exename;

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="file">The path to the obs executable</param>
        public ObsClient(string file) 
        {
            exefile = file;
            exename = Path.GetFileNameWithoutExtension(file);
        }

        /// <summary>
        /// Starts a new OBS Studio instance
        /// </summary>
        public void StartObs()
        {
            Process[] localByName = Process.GetProcessesByName(exename);
            if (localByName.Length <= 0)
            {
                using Process p = new Process();

                p.StartInfo.FileName = exefile;
                p.StartInfo.WorkingDirectory = Path.GetDirectoryName(exefile);
                p.StartInfo.Arguments = "--startvirtualcam --minimize-to-tray --portable --disable-updater --disable-missing-files-check";
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.UseShellExecute = true;
                p.Start();
            }
        }

        /// <summary>
        /// Kills any active OBS Studio instance
        /// </summary>
        public void StopObs()
        {
            Process[] localByName = Process.GetProcessesByName(exename);
            foreach (Process p in localByName)
            {
                try
                {
                    p.Kill();
                }
                finally
                {
                    p.Dispose();
                }
            }
        }

        /// <summary>
        /// Verifies if an OBS Studio instance is active
        /// </summary>
        /// <returns></returns>
        public bool IsStreaming()
        {
            Process[] localByName = Process.GetProcessesByName(exename);
            return (localByName.Length > 0);
        }
    }
}
